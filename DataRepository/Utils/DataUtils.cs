﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DataRepository.Entities;
using Newtonsoft.Json;
using System.Reflection;
using System.Web;
using System.Diagnostics;

namespace DataRepository
{
    public static class DataUtils
    {

        static DataUtils()
        {
           

        }
        private static string Location { get {
                
                return HttpContext.Current.Server.MapPath("~/JsonDataSource/JsonDataSource.json"); //when running from the MVC app
            

        }
        }



        
        public static IEnumerable<Toy> LoadJsonFile()
        {
            List<Toy> toys;
            
            try
            {
                using (StreamReader r = new StreamReader(Location))
                {
                    string json = r.ReadToEnd();
                    toys = JsonConvert.DeserializeObject<List<Toy>>(json);
                }

                return toys;
            }
            
            catch (Exception e)
            {
                throw e;
            }
        }

        public static bool SaveJsonFile(IEnumerable<Toy> myToyList)
        {
            if (myToyList != null && File.Exists(Location))
            {
                try
                {
                    //serialize the list to store
                    var json = JsonConvert.SerializeObject(myToyList);
                    //save json into a file overwriting it
                        File.WriteAllText(Location, json);
                        return true;
                    
                    
                } catch(Exception e)
                {
                    throw new Exception("Error ocurred when saving", e.InnerException);
                }
                
            }

            return false;
        }

    }
}
