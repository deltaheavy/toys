# Toys

This project was a development test from a company who hired me.

These are the tech specs:

Application Requirements
Create a stock management web tool for “Toys and Games” store.
 Should be able to list the available products in a grid
 Should be able to Create, Update, Delete products
 Should provide a simple Form when creating or updating products
 Should provide user confirmation for product Deletion
 (OPTIONAL) display product images
Product data dictionary
Name Type Optional Constrains
Id Int No Unique
Name string No Max length 50
Description string Yes Max length 100
AgeRestriction int Yes 0 to 100
Company string No Max length 50
Price decimal No 1$ to $1000
UI mocks
Feel free to improve or add any extra elements that help usability or user experience, use these mocks as reference
 Product Listing

 Input Form

Architecture constrains
A. Data Persistence
1. Use simple file storage in JSON or XML format
2. Implement IRepository pattern
3. (OPTIONAL) Use seed data
4. (OPTIONAL) Use of an ORM to link the files and the Repository
B. Server Side
1. Use C# with .Net Framework 4.5 (or latest)
2. Use ASP.NET MVC 5 (or latest)
3. Communicate the UI and the backend with a REST API using Web API 2 (or latest)
4. The MVC and WebAPI code can share the same project or live as separate projects
5. Favor IIS express or Kestrel over IIS webapp or website
6. Add a Unit test project with at least 5 unit tests using either MSTest, Xunit or Nunit
7. (OPTIONAL) model/entity validation
8. (OPTIONAL) Use Dependency Injection
9. (OPTIONAL) Use mocking framework such as moq, Rhinomocks or FakeitEasy
10. (OPTIONAL) User .Net Core 1.0 and ASP.Net Core 1.0
11. (OPTIONAL) provide code coverage or cyclometric complexity analysis
C. Client Side
1. Use AJAX to communicate the front end with the REST API backend
2. Use a JavaScript framework like: AngularJS, ReactJS, KnockoutJS, JQuery, KendoUI
3. Use simple CSS or a CSS framework like Bootstrap to provide a simple UI experience
4. (OPTIONAL) Input form validation
5. (OPTIONAL) Add JS unit test using Jasmine, Qunit or any other test framework